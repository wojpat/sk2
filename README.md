# README #

Temat: Serwer memcached. Klient prosi o cos serwer, serwer oddaje z cache jak ma, jesli nie ma zapisuje i oddaje.

Klient prosi serwer o numer lini z pliku, a serwer w odpowiedzi przekazuje zawartość lini.
Użytko protokołu TCP/IP.

Serwer

1. Czeka na połączenia od klientów.
2. Gdy dostaje numer lini sprawdza czy daną linię, ma w cache'u
	2a. Jeśli tak to przesyła ją do klienta
	2b. Jeśli nie to przeszukuje plik z dysku, zapisuje do cache'a i przesyła ją do klienta
Do zapisu i odczytu z cache'a użyto algorytmu LRU.



Pliki serwer:
linkedList.cpp - lista dwukierunkowa użyta do algorytmu lru w cache.cpp
cache.cpp - zapis i odczyt z cache zgodnie z algorytmem lru
common.cpp - funkcje pomocnicze
server.cpp - tworzenie socketa, odbieranie numeru lini, wysyłanie lini do klienta

Pliki klient:
client.cpp - tworzenie socketa przyłączenie do serwera, wysyłanie numeru lini do serwera, odbieranie lini 

Algorytm

Klient
W pętli:
1. Prosi o wprowadzenie numeru lini
2. Tworzy socket i łączy się z serwerem
3. Przesyła do serwera numer lini
4. Czeka na odpowiedź
5. wypisuje numer lini

Serwer

1. Tworzy socket
w pętli:
2. czeka na requesty od klienta
3. odbiera numer lini i sprawdza czy taka linia znajduje się w tablicy haszującej
	3a. Jeśli tak to pobiera wartość z tablicy haszującej, która ma wskaźnik na węzeł w liście zwraca linię i węzeł przeności na koniec listy zgodnie z algorytmem lru
	3b. Jeśli nie to przeszukuje plik na dysku w celu pozystkania danej lini, nastepnie tworzy węzeł z linią i sprawdza czy lista może pomieścić kolejny węzeł
		3ba. Jeśli tak to dodaje węzeł na koniec listy i zapisuje wskaźnik w tablicy haszującej.
		3bb. Jeśli nie to usuwa węzeł z początku listy i z wskaźnik z tablicy haszującej nastepnie dodaje na koniec nowy węzeł i zapisuję wskaźnik w tablicy haszującej
4. Zwraca linię klientowi


Jak zbudować projekt?

Należy zainstalować cmake
sudo apt-get update
sudo apt-get install cmake


Serwer
cd sk2/server
cmake .
make
./MemcashedServer

Klient
cd sk2/clientSk2
cmake .
make
./clientSk2

linie numerowane są o 0.


This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact